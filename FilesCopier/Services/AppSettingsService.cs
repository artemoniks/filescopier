﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace FilesCopier.Services
{
    public class AppSettingsService
    {
        public AppSettingsService()
        {
            DirectoryFrom = ConfigurationManager.AppSettings["DirectoryFrom"];
            DirectoryTo = ConfigurationManager.AppSettings["DirectoryTo"];

            bool additionalSleep;
            bool.TryParse(ConfigurationManager.AppSettings["AdditionalSleep"], out additionalSleep);
            AdditionalSleep = additionalSleep;
        }

        public string DirectoryFrom { get; private set; }
        public string DirectoryTo { get; private set; }
        public bool AdditionalSleep { get; private set; }

    }
}