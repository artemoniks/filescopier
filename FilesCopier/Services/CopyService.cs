﻿using System;
using System.IO;
using System.Threading;

namespace FilesCopier.Services
{
    public class CopyService
    {
        private string _directoryFrom;
        private string _directoryTo;
        private bool _additionalSleep;


        public CopyService()
        {
            var appSettingsService = new AppSettingsService();
            _directoryFrom = appSettingsService.DirectoryFrom;
            _directoryTo = appSettingsService.DirectoryTo;
            _additionalSleep = appSettingsService.AdditionalSleep;
        }

        public int Copy(Stream file, bool onlyCopy, Action<int> returnProgress)
        {
            PrepareContext();

            var copiedFiles = 0;
            var counter = 0;

            var sr = new StreamReader(file);
            var filesStr = sr.ReadToEnd();
            var fileNames = filesStr.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            var percentPart = 100 / fileNames.Length;

            foreach (var name in fileNames)
            {
                ProcessFileName(onlyCopy, name, ref copiedFiles);
                counter++;
                returnProgress(counter * percentPart);

                if (_additionalSleep)
                    Thread.Sleep(1000);
            }

            return copiedFiles;

        }

        private void ProcessFileName(bool onlyCopy, string name, ref int copiedFiles)
        {
            string[] files = Directory.GetFiles(_directoryFrom, name + ".*");

            if (files.Length == 0)
            {
                return;
            }

            foreach (var f in files)
            {
                ProcessFullFileName(onlyCopy, ref copiedFiles, f);
            }
        }

        private void ProcessFullFileName(bool onlyCopy, ref int copiedFiles, string fullFileName)
        {
            var source = fullFileName;
            var dest = GetFullPath(_directoryTo, Path.GetFileName(fullFileName));


            if (File.Exists(fullFileName) == false)
            {
                return;
            }

            if (onlyCopy)
            {
                File.Copy(source, dest, true);
            }
            else
            {
                if (File.Exists(dest))
                {
                    File.Delete(dest);
                }

                File.Move(source, dest);
            }

            copiedFiles++;
        }

        private void PrepareContext()
        {
            if (Directory.Exists(_directoryFrom) == false)
            {
                throw new ApplicationException(string.Format("Директория {0} не существует", _directoryFrom));
            }

            if (Directory.Exists(_directoryTo) == false)
            {
                Directory.CreateDirectory(_directoryTo);
            }

        }

        private string GetFullPath(string path, string fileName)
        {
            return Path.Combine(path, fileName);
        }

    }
}