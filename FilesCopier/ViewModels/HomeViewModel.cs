﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc.Html;

namespace FilesCopier.ViewModels
{
    public class HomeViewModel
    {
        public HomeViewModel()
        {
            OnlyCopy = true;
        }


        [Required, Microsoft.Web.Mvc.FileExtensions(Extensions = "txt",
            ErrorMessage = "Выберите *.txt файл")]
        [Display(Name = "Список файлов:")]
        public HttpPostedFileBase File { get; set; }


        [Display(Name = "Копировать без удаления")]
        public bool OnlyCopy { get; set; }

        public string Message { get; set; }
    
    }
}