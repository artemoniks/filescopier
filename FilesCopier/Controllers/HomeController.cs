﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using FilesCopier.Services;
using FilesCopier.ViewModels;

namespace FilesCopier.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(new HomeViewModel());
        }


        [HttpPost]
        public ActionResult CopyOrMoveFiles(HomeViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var countOfCopied = new CopyService().Copy(viewModel.File.InputStream, viewModel.OnlyCopy, ProgressHub.SendMessage);

                    viewModel.Message = string.Format("Процесс завершен! Обработано {0} файлов", countOfCopied);
                    return View("Index", viewModel);
                }
                else
                {
                    return View("Index", viewModel);
                }
            }
            catch (Exception ex)
            {
                viewModel.Message = ex.Message;
                return View("Index", viewModel);
            }
        }
    }
}
